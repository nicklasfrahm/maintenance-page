# base image
FROM node:8-alpine

# working directory
WORKDIR /usr/src/app

# copy dependency lists
COPY package.json package-lock.json ./

# install dependencies
RUN npm install --production

# copy source files
COPY . ./

# configure dynamic environment variables
ENV PORT=""
ENV MESSAGE=""

# make port accessible
EXPOSE $PORT

# start application
CMD [ "node", "src/index" ]
