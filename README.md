# Maintenance page

A simple web application to display the maintenance status of a web page.

# Installation

Before you begin, create a new .env file with the following content:

```ini
# HTTP server port
PORT=9000
MESSAGE=Under maintenance.
```

Make sure to have at least the latest LTS of [nodejs](https://nodejs.org/en/) installed, then run:

```shell
$ npm i
```

Once you have the dependencies installed, you can run:

```shell
$ npm start
```

Now point your browser to [http://localhost:8080](http://localhost:8080).
