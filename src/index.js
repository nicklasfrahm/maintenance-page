const path = require('path');
const express = require('express');
const helmet = require('helmet');
const dotenv = require('dotenv');

dotenv.config();

const app = express();
const port = process.env.PORT || 8080;
const message = process.env.MESSAGE || 'Under maintenance.';

app.use(helmet());
app.use(express.static(path.resolve('./src/static')));

app.set('view engine', 'pug');
app.set('views', path.resolve('./src/templates'));

app.use((req, res) => {
  return res.status(200).render('index', {
    host: req.headers.host,
    message
  });
});

app.listen(port, () => {
  console.log(`HTTP server online: 0.0.0.0:${port}`);
});
